var gulp = require('gulp'),
	minifycss = require('gulp-minify-css'),
	uglify = require('gulp-uglify'),
	concat = require('gulp-concat'),
	del = require('del');

gulp.task('default', ['clean'], function() {
	gulp.start('styles', 'scripts');
});

gulp.task('clean', function(cb) {
	del('build/*', cb);
});

gulp.task('styles', function() {
	gulp.src([
		'css/pure.css',
		'css/jquery-ui.css',
		'css/flaticon.css',
		'css/colpick.css',
		'css/base.css',
		'css/layout.css',
		'css/modules.css'
	])
		.pipe(minifycss({
			keepSpecialComments: 0
		}))
		.pipe(concat('styles.min.css'))
		.pipe(gulp.dest('build'));
});

gulp.task('scripts', function() {
	gulp.src(
		[
			'js/helpers/*.js',
			'js/vendor/*.js',
			'!js/vendor/jquery-2.1.3.min.js',
			'js/*.js'
		]
	)
		.pipe(uglify({
			preserveComments: 'some'
		}))
		.pipe(concat('scripts.min.js'))
		.pipe(gulp.dest('build'));
});