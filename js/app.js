$(document).ready(function() {
	Panel.init('#panel');
	ColorControl.init('#marker-settings-color');
	Direction.init('#direction');
	Preview.init('#preview');

	Panel.addMarker('#1e5799', 0);
	Panel.addMarker('#7db9e8', 100);
});