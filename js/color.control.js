var ColorControl = {
	init: function(id) {
		this.$el = $(id);
		this.$color = this.$el.find('.marker-settings-color-picker');
		this.$location = this.$el.find('.marker-settings-color-location-input');
		this.$btnDelete = this.$el.find('.marker-settings-color-delete');

		this.marker = null;
		this.color = null;
		this.location = null;
		this.active = true;

		this._setupListeners();
		this.disable();
	},
	setMarker: function(marker, canDelete) {
		this.marker = marker;
		this.enable(canDelete);
		this.update();
		this.$color.colpickSetColor(marker.getColor().substr(1));
	},
	update: function() {
		this.setColor(this.marker.getColor());
		this.setLocation(this.marker.getLocation());
	},
	disable: function() {
		this.$color.addClass('is-disabled');
		this.$location
			.val('')
			.addClass('is-disabled')
			.prop('disabled', true);
		this.$btnDelete.prop('disabled', true);

		this.active = false;

		this.$color.colpickHide();
	},
	enable: function(canDelete) {
		this.$color.removeClass('is-disabled');
		this.$location
			.removeClass('is-disabled')
			.prop('disabled', false);

		if (canDelete) {
			this.$btnDelete.prop('disabled', false);
		}

		this.active = true;
	},
	isActive: function() {
		return this.active;
	},
	setColor: function(color) {
		if (color != this.color) {
			this.color = color;
			this._render();
			this.marker.setColor(color);
		}
	},
	setLocation: function(location) {
		location = parseInt(location) || 0;

		if (location != this.location) {
			this.location = location;
			this._render();
			this.marker.setLocation(location);
		}
	},
	_render: function() {
		this.$color.css('background-color', this.color);
		this.$location.val(this.location);
	},
	_setupListeners: function() {
		var self = this;

		this.$color.colpick({
			onShow: function() {
				if (!self.isActive()) {
					return false;
				}
			},
			onSubmit: $.proxy(this._onColorChange, this)
		});

		this.$location
			.jStepper({
				minValue: 0,
				maxValue: 100,
				allowDecimals: false
			})
			.mousewheel(function(e) {
				self.$location.trigger('keyup');
			})
			.keyup($.proxy(this._onLocationChange, this));
		this.$btnDelete.click($.proxy(this._onDeleteClick, this));
	},
	_onColorChange: function(hsb, hex, rgb, el) {
		this.setColor('#' + hex);
		this.$color.colpickHide();
	},
	_onLocationChange: function() {
		this.setLocation(this.$location.val());
	},
	_onDeleteClick: function() {
		Panel.removeMarker(this.marker);
		this.disable();
	}
};
