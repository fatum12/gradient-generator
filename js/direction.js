var Direction = {
	init: function(id) {
		this.$el = $(id);
		this.$input = this.$el.find('.direction-input');
		this.$buttons = this.$el.find('.btn-direction-preset');
		this.$knob = this.$el.find('.direction-knob-input');

		this.direction = 90;

		this._setupListeners();
		this._render();
	},
	setDirection: function(direction) {
		direction = parseInt(direction) || 0;

		if (this.direction != direction) {
			this.direction = direction;
			this._render();
			Preview.update();
		}
	},
	getDirection: function() {
		return this.direction;
	},
	_render: function() {
		this.$input.val(this.direction);
		this.$knob
			.val(this.direction)
			.trigger('change');
		this.$buttons
			.removeClass('pure-button-selected')
			.filter('[data-value=' + this.direction + ']').addClass('pure-button-selected');
	},
	_setupListeners: function() {
		var self = this;
		this.$input
			.jStepper({
				minValue: 0,
				maxValue: 359,
				allowDecimals: false
			})
			.mousewheel(function(e) {
				self.$input.trigger('keyup');
			})
			.keyup($.proxy(this._onInput, this));
		this.$buttons.click(function() {
			self.setDirection($(this).data('value'));
		});
		this.$knob.knob({
			min: 0,
			max: 359,
			step: 1,
			width: 50,
			height: 50,
			cursor: 10,
			thickness: 0.5,
			fgColor: '#222',
			displayInput: false,
			release: function(value) {
				self.setDirection(value);
			}
		});
	},
	_onInput: function() {
		this.setDirection(this.$input.val());
	}
};