var gradientHelper = {
	prefixDirections: {
		'to right': 'left',
		'to left': 'right',
		'to bottom': 'top',
		'to top': 'bottom'
	},
	wordDirections: {
		90: 'to right',
		270: 'to left',
		180: 'to bottom',
		0: 'to top'
	},
	getCss: function(direction, stops, delimiter) {
		delimiter = delimiter || '';
		var svg = this.getSVG(direction - 180, stops);

		if (direction in this.wordDirections) {
			direction = this.wordDirections[direction];
		}
		var colorsArray = [];
		stops.forEach(function(stop) {
			colorsArray.push(stop.color + ' ' + stop.location + '%');
		});

		var result = '';
		// old browsers
		result += 'background: ' + stops[0].color + ';' + delimiter;
		// svg fallback (IE9)
		result += 'background: url(data:image/svg+xml;base64,' + Base64.encode(svg) + ');' + delimiter;
		// prefixes
		var prefixDirection;
		// direction in degrees
		if (parseInt(direction) === direction) {
			prefixDirection = (90 - direction) + 'deg';
			direction += 'deg';
		}
		else {
			if (direction in this.prefixDirections) {
				prefixDirection = this.prefixDirections[direction];
			}
			else {
				prefixDirection = direction;
			}
		}

		['webkit', 'moz', 'o'].forEach(function(prefix) {
			result += 'background: -' + prefix + '-linear-gradient(' + prefixDirection + ', ' + colorsArray.join(', ') + ');' + delimiter;
		});
		result += 'background: linear-gradient(' + direction + ', ' + colorsArray.join(', ') + ');';

		return result;
	},
	getSVG: function(direction, stops) {
		var svg = '<svg xmlns="http://www.w3.org/2000/svg" version="1.1" width="100%" height="100%">';
		svg += '<defs>';
		var points = this.angleToPoints(direction);
		svg += '<linearGradient id="g1" gradientUnits="userSpaceOnUse" x1="' + points.x1 + '%" y1="' +
			points.y1 + '%" x2="' + points.x2 + '%" y2="' + points.y2 + '%">';
		stops.forEach(function(stop) {
			svg += '<stop offset="' + stop.location + '%" stop-color="' + stop.color + '"/>';
		});
		svg += '</linearGradient>';
		svg += '</defs>';
		svg += '<rect x="0" y="0" width="100%" height="100%" fill="url(#g1)"/>';
		svg += '</svg>';

		return svg;
	},
	angleToPoints: function (angle) {
		angle = angle % 360;
		if (angle < 0) {
			angle = 360 + angle;
		}

		var start_x = 0,
			end_x = 0,
			start_y = 0,
			end_y = 100,
			angle_delta;

		if (angle >= 0 && angle <= 45) {
			start_x = ((angle / 45) * 50) + 50;
			end_x = 100 - start_x;
			start_y = 0;
			end_y = 100;
		}
		else if (angle > 45 && angle <= 135) {
			angle_delta = angle - 45;
			start_x = 100;
			end_x = 0;
			start_y = (angle_delta / 90) * 100;
			end_y = 100 - start_y;
		}
		else if (angle > 135 && angle <= 225) {
			angle_delta = angle - 135;
			start_x = 100 - ((angle_delta / 90) * 100);
			end_x = 100 - start_x;
			start_y = 100;
			end_y = 0;
		}
		else if (angle > 225 && angle <= 315) {
			angle_delta = angle - 225;
			start_x = 0;
			end_x = 100;
			start_y = 100 - ((angle_delta / 90) * 100);
			end_y = 100 - start_y;
		}
		else if (angle > 315 && angle <= 360) {
			angle_delta = angle - 315;
			start_x = (angle_delta / 90) * 100;
			end_x = 100 - start_x;
			start_y = 0;
			end_y = 100;
		}

		var round = function(num) {
			return Math.round(num * 10) / 10;
		};

		return {
			x1: round(start_x),
			y1: round(start_y),
			x2: round(end_x),
			y2: round(end_y)
		};
	}
};