var Marker = function(color, location, panel) {
	this.color = color;
	this.location = location;
	this.panel = panel;
	this.active = false;

	// создаем представление маркера
	this.$el = $('<div class="panel-marker"></div>').appendTo(this.panel.getColorMarkersContainer());

	this._setupListeners();
	this._render();
};

Marker.prototype = {
	activate: function() {
		this.$el.addClass('is-active');
		this.active = true;
	},
	deactivate: function() {
		this.$el.removeClass('is-active');
		this.active = false;
	},
	remove: function() {
		this.$el.remove();
		this.panel = null;
	},
	isActive: function() {
		return this.active;
	},
	setColor: function(color) {
		if (color != this.color) {
			this.color = color;
			this._render();
			this.panel.update();
		}
	},
	getColor: function() {
		return this.color;
	},
	setLocation: function(location) {
		location = parseInt(location) || 0;

		if (location != this.location) {
			this.location = location;
			this._render();
			this.panel.update();
		}
	},
	getLocation: function() {
		return this.location;
	},
	reverse: function() {
		this.setLocation(100 - this.location);
	},
	_render: function() {
		var panelWidth = this.panel.getWidth(),
			offset = Math.floor(panelWidth * (this.location / 100));

		this.$el.css({
			backgroundColor: this.color,
			left: offset
		});
	},
	_setupListeners: function() {
		this._draggable();
		this.$el.mousedown($.proxy(this._onClick, this));
	},
	_draggable: function() {
		var self = this;
		this.$el.draggable({
			addClasses: false,
			containment: 'parent',
			axis: 'x',
			drag: function(event, ui) {
				self.setLocation(Math.round(ui.position.left * 100 / self.panel.getWidth()));
			}
		});
	},
	_onClick: function(e) {
		e.preventDefault();
		if (!this.isActive()) {
			this.panel.setActiveColorMarker(this);
		}
	}
};