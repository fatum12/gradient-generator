var Panel = {
	init: function(id) {
		this.$el = $(id);
		this.$gradient = this.$el.find('.panel-line');
		this.$colorMarkersContainer = this.$el.find('.panel-markers');
		this.$btnReverse = this.$el.find('.panel-reverse');

		this.colorMarkers = [];
		this.activeColorMarker = null;

		this._setupListeners();
	},
	update: function() {
		if (this.activeColorMarker) {
			ColorControl.update();
		}

		this.$gradient.attr('style', gradientHelper.getCss(90, this.getStops()));

		Preview.update();
	},
	getStops: function() {
		var stops = [];
		this.colorMarkers.forEach(function(marker) {
			stops.push({
				location: marker.getLocation(),
				color: marker.getColor()
			});
		});

		// сортируем цвета по возрастанию location
		stops.sort(function(a, b) {
			if (a.location < b.location) {
				return -1;
			}
			if (a.location > b.location) {
				return 1;
			}
			return 0;
		});

		return stops;
	},
	getWidth: function() {
		return this.$el.width();
	},
	addMarker: function(color, location) {
		var marker = new Marker(color, location, this);
		this.colorMarkers.push(marker);

		this.update();

		return marker;
	},
	removeMarker: function(marker) {
		marker.remove();
		if (marker === this.activeColorMarker) {
			this.activeColorMarker = null;
		}
		var index = this.colorMarkers.indexOf(marker);
		if (index != -1) {
			this.colorMarkers.splice(index, 1);
		}
		this.update();
	},
	getColorMarkersContainer: function() {
		return this.$colorMarkersContainer;
	},
	setActiveColorMarker: function(marker) {
		if (this.activeColorMarker !== null) {
			this.activeColorMarker.deactivate();
		}
		this.activeColorMarker = marker;
		marker.activate();
		ColorControl.setMarker(marker, this.colorMarkers.length > 2);
	},
	reverse: function() {
		this.colorMarkers.forEach(function(marker) {
			marker.reverse();
		});
	},
	_setupListeners: function() {
		var self = this;
		this.$gradient.click($.proxy(this._onClick, this));
		this.$btnReverse.click(function(e) {
			e.preventDefault();
			self.reverse();
		});
	},
	_onClick: function(e) {
		e.preventDefault();

		var offset = this.$gradient.offset(),
			x = e.clientX - offset.left;

		var marker = this.addMarker(this.colorMarkers[0].getColor(), Math.floor(x * 100 / this.getWidth()));

		this.setActiveColorMarker(marker);
	}
};