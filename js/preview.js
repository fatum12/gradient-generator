var Preview = {
	init: function(id) {
		this.$el = $(id);
		this.$gradient = this.$el.find('.preview-panel-gradient');
		this.$code = this.$el.find('.preview-css-code');
		this.$btnCopy = this.$el.find('.preview-css-copy');

		this._setupListeners();
	},
	update: function() {
		var css = gradientHelper.getCss(Direction.getDirection(), Panel.getStops(), "\n");

		this.$gradient.attr('style', css.replace(/\n/g, ''));
		this.$code.val($.trim(css));
	},
	_setupListeners: function() {
		if (ZeroClipboard.isFlashUnusable()) {
			this.$btnCopy.hide();
		}
		else {
			new ZeroClipboard(this.$btnCopy);
		}
	}
};